﻿using ProjectCore.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectCore.Repositories
{
    public interface IUserRepository : IGenericRepository<User>
    {
    }
}
